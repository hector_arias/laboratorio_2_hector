/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package laboratorio_2_hector;

import java.util.Scanner;

/**
 *
 * @author Hector_Arias
 */
public class Laboratorio_2_Hector {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        boolean continuar = true;
        while (continuar) {
            System.out.println("---------------Bienvenidos-----------------");
            System.out.println("1- Registro de Personas" + "\n"
                    + "2- Registro de Mascotas" + "\n"
                    + "3- Adoptar Mascota" + "\n"
                    + "4- Salir");
            Scanner var = new Scanner(System.in);
            int opcion_principal = 0;
            System.out.println("Elija una opcion por favor:");
            opcion_principal = var.nextInt();
            if (opcion_principal < 1 || opcion_principal > 4) {

                System.out.println("Opcion no disponible." + "\n");
                continuar = true;
            } else if (opcion_principal == 1) {

                Registro_Persona a = new Registro_Persona();
                a.registrarLista_Persona();

            } else if (opcion_principal == 2) {
                Registro_Mascota b = new Registro_Mascota();
                b.registrarLista_Mascota();

            } else if (opcion_principal == 3) {
                Registrar_Adopcion c = new Registrar_Adopcion();
                c.buscarCedula();
            }
        }
    }
}
